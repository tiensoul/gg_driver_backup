from datetime import datetime
import shutil
import json
import os
from Modules.driver import *

# load config
with open("config.json") as config:
    data = json.load(config)

backup_path = data['backup_path']
driver_backup_path = data['driver_backup_path']

#zip folder
def zipfolder(file_name, backup_path):
    if os.path.isdir(backup_path):
        print("%s\\%s" % (backup_path, file_name))
        shutil.make_archive("backup\\%s" % file_name, "zip", backup_path)
    else:
        return


file_name = datetime.today().strftime('outline-%Y%m%d')
zipfolder(file_name, backup_path)

#upload to driver
file_name += ".zip"
file = "backup\\{0}".format(file_name)
upload(main(), file_name, file, driver_backup_path)