from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaFileUpload
from apiclient import errors
from datetime import datetime, timedelta, date


# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/drive']


def main():
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('/../token.pickle'):
        with open('../token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('../token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)
    return service


def upload(service, name_file, path_file, path_driver):
    # Call the Drive v3 API
    results = service.files().list(fields="nextPageToken, files(id, name, createdTime)").execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
    else:
        print('Files:')
        for item in items:
            name_item = item['name']
            print(item['createdTime']);
            if name_item == path_driver:
                # insert file in a folder
                folder_id = item['id']
                file_metadata = {
                    'name': name_file,
                    'parents': [folder_id]
                }

                media = MediaFileUpload(path_file, mimetype='application/zip', resumable=True)
                service.files().create(body=file_metadata, media_body=media, fields='id').execute()
                print('** Uploaded "%s" to "%s"' % (name_file, path_driver))

            date_created = name_item.find('outline-')
            extend_file = name_item.rfind('.zip')
            if date_created != (-1) and extend_file != (-1):
                length = len(name_item) - 4
                datetime_str = name_item[date_created + 8:length]
                date_file = date(year=int(datetime_str[0:4]), month=int(datetime_str[4:6]), day=int(datetime_str[-2:]))
                now = datetime.today() + timedelta(days=7)
                today = date(year=now.year, month=now.month, day=now.day)
                if today > date_file:
                    delete_file(service, item['id'])
                    print('Deleted file %s (%s)' % (name_item, item['id']))


def delete_file(service, file_id):
    try:
        service.files().delete(fileId=file_id).execute()
    except errors.HttpError, error:
        print('An error occurred: %s' % error)
